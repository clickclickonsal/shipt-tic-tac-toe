import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import ScoreBoard from '../../../src/components/TicTacToeBoard/ScoreBoard';

it('renders correctly', () => {
  const tree = renderer
    .create(<ScoreBoard turn="X" XWins={0} OWins={0} ties={0} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
