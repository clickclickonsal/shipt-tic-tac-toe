import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import Square from '../../../src/components/TicTacToeBoard/Square';

it('renders correctly', () => {
  const tree = renderer
    .create(<Square disabled={false} value="O" onPress={() => {}} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});
