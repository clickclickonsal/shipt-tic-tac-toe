import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import TicTacToeBoard from '../../../src/components/TicTacToeBoard/TicTacToeBoard';

it('renders correctly', () => {
  const tree = renderer.create(<TicTacToeBoard />).toJSON();

  expect(tree).toMatchSnapshot();
});
