import 'react-native';

import checkIfWinner from '../../../src/components/TicTacToeBoard/checkIfWinner';

it("returns true if board is ['X', 'X', 'X', 'O', 'O']", () => {
  expect(checkIfWinner(['X', 'X', 'X', 'O', 'O'], 'X')).toBe(true);
});

it("returns true if board is ['X', 'O', 'X', 'O', 'O', 'X', 'O', 'O', 'X']", () => {
  expect(
    checkIfWinner(['X', 'O', 'X', 'O', 'O', 'X', 'O', 'O', 'X'], 'X'),
  ).toBe(true);
});

it('returns false if there are less than 5 plays ', () => {
  expect(checkIfWinner(['X', 'O', 'O'], 'X')).toBe(false);
});

it('returns tie if there are 9 plays & no winner is declared', () => {
  expect(
    checkIfWinner(['X', 'O', 'O', 'X', 'O', 'O', 'O', 'X', 'O'], 'X'),
  ).toBe('tie');
});
