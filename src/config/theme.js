import EStyleSheet from 'react-native-extended-stylesheet';

const theme = {
  $theme: 'light',
  $mainBackgroundColor: '#FFFFFF',
  $primaryColor: '#000000',
  $secondaryColor: '#42C0FB',
  $textColor: '#000000',
  $secondaryTextColor: '#FFFFFF',

  $playerOneColor: 'red',
  $playerTwoColor: 'blue',
};

EStyleSheet.build(theme);
