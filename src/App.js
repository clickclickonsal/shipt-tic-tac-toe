import React from 'react';
import { SafeAreaView, View, StatusBar, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import './config/theme'; // initialize react-native-extended-styleSheet

import TicTacToeBoard from './components/TicTacToeBoard';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.body}>
          <TicTacToeBoard />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = EStyleSheet.create({
  body: {
    height: '100%',
    width: '100%',
    backgroundColor: '$mainBackgroundColor',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
