import React from 'react';
import PropTypes from 'prop-types';
import { Alert, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import Square from './Square';
import ScoreBoard from './ScoreBoard';
import Button from '../Button';
import Spacer from '../Spacer';

import checkIfWinner from './checkIfWinner';

class TicTacToeBoard extends React.Component {
  static defaultProps = {
    winnables: [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ],
  };

  static propTypes = {
    winnables: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
  };

  state = {
    grid: ['', '', '', '', '', '', '', '', ''],
    turn: 'X',
    winner: null,
    isTie: false,
    OWins: 0,
    XWins: 0,
    ties: 0,
  };

  resetGame = () => {
    this.setState({
      grid: ['', '', '', '', '', '', '', '', ''],
      turn: this.state.winner || this.state.turn,
      winner: null,
      isTie: false,
    });
  };

  gameEndedAlert = () => {
    const { isTie, winner } = this.state;

    Alert.alert(
      isTie ? "It's a Tie!" : `Player ${winner} wins!`,
      '',
      [
        {
          text: 'Dismiss',
          onPress: () => {},
          style: 'cancel',
        },
        { text: 'Play Again', onPress: this.resetGame },
      ],
      { cancelable: false },
    );
  };

  checkBoardIfWinner = player => {
    const result = checkIfWinner(this.state.grid, player);

    if (result === 'tie') {
      this.setState(
        { isTie: true, ties: (this.state.ties += 1) },
        this.gameEndedAlert,
      );
    }

    if (result) {
      const playerWins = `${player}Wins`;

      this.setState(
        {
          winner: player,
          [playerWins]: (this.state[playerWins] += 1),
        },
        this.gameEndedAlert,
      );
      return;
    }
  };

  onPlay = (player, spaceIndex) => {
    const updatedGrid = [...this.state.grid];
    updatedGrid[spaceIndex] = player;

    const updatedState = {
      grid: updatedGrid,
      turn: player === 'X' ? 'O' : 'X',
    };

    this.setState(updatedState, () => {
      this.checkBoardIfWinner(player);
    });
  };

  render() {
    const { turn, grid, winner, isTie, XWins, OWins, ties } = this.state;

    return (
      <>
        <View style={styles.board}>
          {grid.map((item, index) => (
            <Square
              key={`square-${index}`}
              value={item}
              disabled={Boolean(item) || Boolean(winner) || isTie}
              onPress={() => this.onPlay(turn, index)}
              stylesOverride={[styles.square, styles[`square-${index}`]]}
              textStylesOverride={[styles.text]}
            />
          ))}
        </View>
        <Spacer />
        <ScoreBoard turn={turn} XWins={XWins} OWins={OWins} ties={ties} />
        <Spacer />
        <Button onPress={this.resetGame} title="New Game" />
      </>
    );
  }
}

const styles = EStyleSheet.create({
  $boardSize: 300,
  $squareSize: '$boardSize / 3',

  board: {
    height: '$boardSize',
    width: '$boardSize',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    borderColor: '$primaryColor',
  },
  square: {
    height: '$squareSize',
    width: '$squareSize',
  },
  'square-0': {
    borderTopWidth: 0,
    borderLeftWidth: 0,
  },
  'square-1': {
    borderTopWidth: 0,
  },
  'square-2': {
    borderTopWidth: 0,
    borderRightWidth: 0,
  },
  'square-3': {
    borderLeftWidth: 0,
  },
  'square-4': {},
  'square-5': {
    borderRightWidth: 0,
  },
  'square-6': {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
  },
  'square-7': {
    borderBottomWidth: 0,
  },
  'square-8': {
    borderBottomWidth: 0,
    borderRightWidth: 0,
  },
  text: {
    fontSize: '$squareSize /2',
  },
});

export default TicTacToeBoard;
