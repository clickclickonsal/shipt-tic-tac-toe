import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

const Score = props => {
  const style = { ...styles.title };

  if (props.color) {
    style.color = props.color;
  }

  if (props.bold) {
    style.fontWeight = 'bold';
  }

  return (
    <View style={styles.scoreContainer}>
      <Text style={style}>{props.title}</Text>
      <Text style={styles.total}>{props.total}</Text>
    </View>
  );
};

const ScoreBoard = props => {
  const { XWins, OWins, ties, turn } = props;

  return (
    <View style={styles.container}>
      <Score
        color={EStyleSheet.value('$playerOneColor')}
        title="Player 1 (X)"
        bold={turn === 'X'}
        total={XWins}
      />
      <Score title="Tie" total={ties} />
      <Score
        color={EStyleSheet.value('$playerTwoColor')}
        title="Player 2 (O)"
        bold={turn === 'O'}
        total={OWins}
      />
    </View>
  );
};

ScoreBoard.propTypes = {
  turn: PropTypes.string.isRequired,
  XWins: PropTypes.number.isRequired,
  OWins: PropTypes.number.isRequired,
  ties: PropTypes.number.isRequired,
};

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  scoreContainer: {
    width: '33%',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
  },
  total: {
    fontSize: 20,
  },
});

export default ScoreBoard;
