import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

const Square = props => {
  const {
    disabled,
    value,
    onPress,
    stylesOverride = [],
    textStylesOverride = [],
  } = props;

  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <View style={[styles.square, ...stylesOverride]}>
        <Text style={[styles[value], ...textStylesOverride]}>{value}</Text>
      </View>
    </TouchableOpacity>
  );
};

Square.propTypes = {
  disabled: PropTypes.bool.isRequired,
  value: PropTypes.oneOf(['X', 'O', '']).isRequired,
  onPress: PropTypes.func.isRequired,
  stylesOverride: PropTypes.arrayOf(PropTypes.object),
  textStylesOverride: PropTypes.arrayOf(PropTypes.object),
};

const styles = EStyleSheet.create({
  square: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  X: {
    color: '$playerOneColor',
  },
  O: {
    color: '$playerTwoColor',
  },
});

export default Square;
