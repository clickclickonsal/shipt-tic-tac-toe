const winnables = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

const getPlays = grid => {
  return grid.reduce(
    (acc, play, index) => {
      if (play) {
        acc[play].push(index);
        acc.total += 1;
      }

      return acc;
    },
    { total: 0, X: [], O: [] },
  );
};

const checkIfWinner = (grid, player) => {
  const plays = getPlays(grid);

  // the minimum amount of plays needed to win is 5 so if there are less than that
  // then there is no need to check any further
  if (plays.total < 5) {
    return false;
  }

  for (let i = 0; i < winnables.length; i++) {
    const winnable = winnables[i];

    if (winnable.every(space => plays[player].includes(space))) {
      return true;
    }
  }

  // at this point if our board is filled & we don't have a winner then we have a tie
  if (plays.total === 9) {
    return 'tie';
  }

  return false;
};

export default checkIfWinner;
