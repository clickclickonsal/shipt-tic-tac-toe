import React from 'react';
import { Button as RNButton, View, Platform } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Button = props => {
  return (
    <View style={{ ...styles.button, ...props.style }}>
      <RNButton
        style={styles.button}
        color={
          Platform.OS === 'android'
            ? EStyleSheet.value('$secondaryColor')
            : EStyleSheet.value('$secondaryTextColor')
        }
        {...props}
      />
    </View>
  );
};

const styles = EStyleSheet.create({
  button: {
    backgroundColor: '$secondaryColor',
  },
});

export default Button;
