import React from 'react';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Spacer = props => {
  return <View style={styles.spacer} />;
};

const styles = EStyleSheet.create({
  spacer: {
    width: '100%',
    padding: 10,
  },
});

export default Spacer;
