# Shipt Tic Tac Toe

- [About](#about)
- [Getting Started](#getting-started)

## About

The classic [Tic Tac Toe](https://en.wikipedia.org/wiki/Tic-tac-toe#Rules) now brought to you on your mobile phone!

## Challenge Requirements

- Allow 2 players to play tic tac toe (Follow the rules of tic tac toe)
- Have 3x3 grid on which the players can play
- Allow the players to take turns marking spaces on the 3x3 grid
- Recognize when a player has won and declare that player as victorious

Added on features

- Allow the user to play again after game ends
- Allow the user to reset the game during game play
- Allow the user to reset the scores
- Keep track of the player wins & ties

Constraints

- Player names can not be changed
- The game requires 2 players, we could eventually build a "Play CPU" option
- the scores will clear when you close/reopen the app
- You cannot clear the score board unless you close/reopen the app

## Getting Started

1. Install Dependencies

```bash
npm install
```

## Running the App

The easiest way to run the app is via the `react-native run-*` command. Replace the star with the platform you want to run on:

```bash
react-native run-ios
react-native run-android
```

You should notice another terminal pop up. This is the React Native packager. If you ever have to stop this, run `npm start` to restart.

## Running the Test Suite

```bash
npm run test
```

## Preview

![Android Preview](https://i.imgur.com/k1DEjzG.png)
![iOS Preview](https://i.imgur.com/RmKTm9h.png)
